"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const data_source_1 = __importDefault(require("../database/data-source"));
const User_1 = __importDefault(require("../models/User"));
const Profile_1 = __importDefault(require("../models/Profile"));
class UserController {
    static create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = new User_1.default();
            const profile = new Profile_1.default();
            user.profile = profile;
            profile.user = user;
            user.email = req.body.email;
            profile.name = req.body.name;
            yield data_source_1.default.manager.save(user);
            yield data_source_1.default.manager.save(profile);
            res.status(200).send({ accessToken: user.accessToken });
        });
    }
    static getAccessToken(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const foundedUser = yield data_source_1.default
                .getRepository(User_1.default)
                .createQueryBuilder("user")
                .where("user.email");
        });
    }
}
exports.default = UserController;
//# sourceMappingURL=UserController.js.map